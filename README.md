# apex-export

Script exports APEX application as set of sql scripts via wwv_flow_utilities package.

Run `apex-export.py -h` to see usage of the script.
