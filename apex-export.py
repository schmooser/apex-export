#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Module: apex-export
Description: Exports apex application as set of sql scripts via wwv_flow_utilities package
"""

import os
import argparse
import cx_Oracle

VERSION=1.1
DESCRIPTION='Script for exporting APEX pages and applications into set of sql files'

first_call=True # indicates first call to wwv_flow_utilities during run

# sed commands which drops unused stuff from file (timestamps, instance ids etc)
sed_commands=['sed -i -s "/^--.*Date and Time.*$/d" %s', 'sed -i -s "/^--.*Instance ID.*$/d" %s']

def export_app(cursor, app_id, output_dir):
    q = "SELECT wwv_flow_utilities.export_application_to_clob(:1) FROM dual"

    global first_call
    if first_call:
        cursor.execute(q, [app_id]) # first fetch is suppressing output of Content-Type
        cursor.fetchone()
        first_call=False

    cursor.execute(q, [app_id])
    b, = cursor.fetchone()
    blob_data = b.read()

    filename = os.path.join(output_dir,'app%d.sql' % app_id)
    f = open(filename,'w')
    f.write(blob_data)
    f.close()
    map(lambda cmd: os.system(cmd % filename), sed_commands)

def export_page(cursor, app_id, page_id, output_dir):
    q = "SELECT wwv_flow_utilities.export_page_to_clob(:1, :2) FROM dual" # :1 - app_id, :2 - page_id

    global first_call
    if first_call:
        cursor.execute(q, [app_id, page_id]) # first fetch is suppressing output of Content-Type
        cursor.fetchone()
        first_call=False
    cursor.execute(q, [app_id, page_id])
    b, = cursor.fetchone()
    blob_data = b.read()

    filename = os.path.join(output_dir,'app%d_page%d.sql' % (app_id, page_id))
    f = open(filename,'w')
    f.write(blob_data)
    f.close()
    map(lambda cmd: os.system(cmd % filename), sed_commands)

def export_app_pages(cursor, app_id, output_dir):
    q = "SELECT page_id FROM apex_application_pages WHERE application_id = :1"
    
    cursor.execute(q, [app_id])
    all = cursor.fetchall()
    for page_id, in all:
        export_page(cursor, app_id, int(page_id), output_dir)

def main():

    # arguments
    argparser = argparse.ArgumentParser(description='%s v%s - %s' % (__file__, VERSION, DESCRIPTION))
    argparser.add_argument('--credentials', required=True,
        help='Credentials for database access in form user/pass@db')

    argparser.add_argument('--mode', default='page',
        help="Export mode. 'page' for exporting pages, 'app' for exporting application")

    argparser.add_argument('--app_id', type=int, required=True, help='App ID')
    argparser.add_argument('--page_id', type=int, help='Page ID')

    argparser.add_argument('--output_dir', default='output',
        help='Output directory, where files will be placed. Will be emptied before export')

    args = argparser.parse_args()

    # make output_dir
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    # exporting
    connection = cx_Oracle.connect(args.credentials)
    connection.autocommit = 0

    cursor = connection.cursor()

    if args.mode=='page':
        if args.page_id: export_page(cursor, args.app_id, args.page_id, args.output_dir)
        else: export_app_pages(cursor, args.app_id, args.output_dir)
    else: export_app(cursor, args.app_id, args.output_dir)

    print 'Export completed successfully'

if __name__ == '__main__':
    main()
